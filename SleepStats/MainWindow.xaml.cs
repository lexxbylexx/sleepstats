﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SleepStats
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            SystemEvents.PowerModeChanged += OnPowerChange;
            
            
            textbox.Text = ReadFile();
            
            
        }
        
        //метод событий, показывающих гибернацию
        private void OnPowerChange(object s, PowerModeChangedEventArgs e)
        {
            switch (e.Mode)
            {
                case PowerModes.Resume:
                    WriteFile("on: "+DateTime.Now.ToString());
                    break;
                case PowerModes.Suspend:
                    WriteFile("off: " + DateTime.Now.ToString());
                    break;
            }
            textbox.Text = ReadFile();
        }

        //запись в файл
        private void WriteFile(string s)
        {
            try
            {
                StreamWriter sw = new StreamWriter("D:\\Test.txt",true);
                sw.WriteLine(s);
                sw.Close();
            }
            catch (Exception e)
            {
                textbox.Text = "Exception: " + e.Message;
            }

        }

        //чтение из файла
        private string ReadFile()
        {
            String line, s = "";
            try
            {
                StreamReader sr = new StreamReader("D:\\Test.txt");
                line = sr.ReadLine();

                while (line != null)
                {
                    line = sr.ReadLine();
                    s += line + "\n";
                }
                sr.Close();
                Console.WriteLine(s);
                return s;
            }
            catch (Exception e)
            {
                return "Exception: " + e.Message;
            }
            
        }

       
    }

}
